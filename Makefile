CC = gcc
CFlags = -g -Wall -Werror -std=c99

hello:
	echo "compiling ...."
	$(CC) $(CFlags) hello.c -o hello
clean:
	echo "cleaning ..."
	- rm hello
run: clean hello
	./hello
